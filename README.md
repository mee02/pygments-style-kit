# KIT Pygments style #
This package adds styles to Pygments which match the corporate identity of
Karlsruhe Insitute of technology.

# License #
The colors used are owned by KIT. All rights reserved.
